function [cvtrain,cvtest] = cvfolds(data,folds)
    dataRowNumber = size(data,1);
    rowsPerFolds = dataRowNumber/folds;
    
    cvtrain = [];
    cvtest = [];
    
    for startOfRow = 1:rowsPerFolds : dataRowNumber
        testRows = startOfRow:startOfRow+rowsPerFolds-1;
        
        if(startOfRow == 1)
            trainRows = [max(testRows)+1 : dataRowNumber];
        else
            trainRows = [1:startOfRow-1 max(testRows)+1:dataRowNumber];
        end
   
        cvtrain = [cvtrain; data(trainRows, :)];
        cvtest = [cvtest; data(testRows, :)];
    end
    
    