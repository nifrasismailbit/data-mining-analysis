N=10;
data = load('shuffledIris.txt');

[cvtrain, cvtest ] = cvfolds(data,N);

r1 = size(cvtrain,1)/N;
r2 = size(cvtest,1)/N;

for fold = 1:N 
    trData = cvtrain(1:r1, :);
    cvtrain(1:r1, :) = [];
    
    teData = cvtest(1:r2, :);
    cvtest(1:r2, :) = [];
    rate1(fold) = knn(trData,teData,3);
end

disp(rate);
