function rate = knn(trData,teData,k)
    m = size(teData,1); % size of the test data in one diamension
    n = size(trData,1);
   
    for test = 1:m
        d=[];
        for train = 1:n
            d(train)=norm(teData(test,1:end-1)-trData(train,1:end-1)); 
        end
        [elts,indices] = sort(d);  % Getting the least elements and their indices
        pos = indices(1:k);              % Getting k nearest neighbohrs
        cls = trData(pos,end); % minimum classes labels
        predict(test) = mode(cls);   % predict the label from trained one

    end
    actual = teData(:,end); % actual label of the data 
    rate =100* sum(predict==actual')/m;
 

    
    