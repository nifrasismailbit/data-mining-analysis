function rate= knn(trdata,tedata,k)
m = size(tedata, 1);
n = size(trdata, 1);

predict = [];
for test= 1:m
    d = [];
    for train =1:n
        d(train) = norm(tedata(test, 1:end-1) - trdata(train, 1:end-1));
    end
     
    [elt,indices]=sort(d);
    pos=indices(1:k);
    cls=trdata(pos,end);
    predict(test)=mode(cls);   
end

[elts,indices]=sort(predict);
actual = tedata(:,end);
rate = 100*sum(predict == actual')/m;
%disp(rate);
