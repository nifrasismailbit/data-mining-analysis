datasets = {'iris','glass','vow','vehicle','wine'};
partition = [0.3,0.5,0.7,0.3];

for i=1:length(datasets)
    dataLink = strcat('data/',datasets(i),'.txt');
    dataLink = strjoin(dataLink);
    data = load(dataLink);
    data= shuffleData(data);
    dataName = strcat(datasets(i),'.txt');
    dataName = strjoin(dataName);
    save(dataName,'data','-ASCII');
       
end
