datasets = {'iris','glass','vow','vehicle','wine'};
classes = [3,6,11,4,3];
partition = [0.3,0.5,0.7,0.3];
kValues = [3,5,7];

c=[2^-10,2^-9,2^-8,2^-7,2^-6,2^-5,2^-4,2^-3,2^-2,2^-1,2^0,2^1,2^2,2^3,2^4,2^5,2^6,2^7,2^8,2^9,2^10];
gama=[2^-10,2^-9,2^-8,2^-7,2^-6,2^-5,2^-4,2^-3,2^-2,2^-1,2^0,2^1,2^2,2^3,2^4,2^5,2^6,2^7,2^8,2^9,2^10];    


%Rates Array Initialization
rateKnn = zeros(length(datasets),length(partition),length(kValues));
properOVACValues = zeros(length(datasets),length(partition));
properRBFCValues = zeros(length(datasets),length(partition));
properRBFGammaValues = zeros(length(datasets),length(partition));

timeKnn =  zeros(length(datasets),length(partition),length(kValues));
timeRBF =  zeros(length(datasets),length(partition));
timeOVA =  zeros(length(datasets),length(partition));


for i=1:length(datasets)
    dataLink = strcat('dataTT/',datasets(i),'.txt');
    dataLink = strjoin(dataLink);
    data = load(dataLink);
    %data= shuffleData(data);
    
    for j=1:length(partition)
        [train,test] = splitData(data,partition(j));
        
        for k=1:length(kValues)
           tic
           rateKnn(i,j,k) = knn(train,test,k);
           timeKnn(i,j,k) = toc;
        end
        

%         tic
%         [maximum,Index] = max(ova(train,test,classes(i)));
%         properC(i,j) = c(Index);
%         timeOVA(i,j) = toc;
        
        
%         
%         tic
%         results = rbf(train,test,classes(i));
%                 
%         [M,I] = max(results(:));
%         [CI, GI] = ind2sub(size(results),I);
%         
%         properRBFCValues(i,j) = c(CI);
%         properRBFGammaValues(i,j) = gama(GI);
%         
%         timeRBF(i,j) = toc;
        
        
        
    end
    
end


