function rate_RBF=rbf(trData,teData,N)
    A=1:N;

    c=[2^-10,2^-9,2^-8,2^-7,2^-6,2^-5,2^-4,2^-3,2^-2,2^-1,2^0,2^1,2^2,2^3,2^4,2^5,2^6,2^7,2^8,2^9,2^10];
    gama=[2^-10,2^-9,2^-8,2^-7,2^-6,2^-5,2^-4,2^-3,2^-2,2^-1,2^0,2^1,2^2,2^3,2^4,2^5,2^6,2^7,2^8,2^9,2^10];
    

    %training
    for i=1:length(c)
        for j=1:length(gama)
            predict=[];
            for cls=1:N
                model=['model' int2str(cls) 'vsAll'];
                options=svmlopt('C',c(i),'Kernel',2,'KernelParam',gama(j),'Verbosity',0);
                trX=invertData(trData,cls);
                trY=trX(:,end);
                trX(:,end)=[];
                svmlwrite('SVMLTrain',trX,trY);
                svm_learn(options,'SVMLTrain',model);

                %testing
                teX=invertData(teData,cls);
                teY=teX(:,end);
                teX(:,end)=[];
                svmlwrite('SVMLTest',teX,teY);
                modeloutput=['modeloutput' int2str(cls) 'vsAll'];
                svm_classify(options,'SVMLTest',model,modeloutput);
                svmpredict=svmlread(modeloutput);
                predict=[predict,svmpredict];

            end   
            rate_RBF(i,j)=WinnerTakesAll(teData,predict,A);

        end

    end
