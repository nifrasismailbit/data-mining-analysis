function[trData,teData]=splitData(data,p)
r=size(data,1);% number of data
m=round(r*p);%  70%of the data
trData=data(1:m,:);% getting the trainning data
data(1:m,:)=[];% delete the 70% of data
teData=data;% copy to teData