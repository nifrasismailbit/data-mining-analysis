function data=invertData(data,cls)
n=size(data,2);%interest class to +1
indices=(data(:,n)==cls);
data(indices,n)=1;%non_interest class(es) to -1;
indices=(data(:,n)~=cls);
data(indices,n)=-1;