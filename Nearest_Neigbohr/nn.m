data=load('iris.txt');
data=shuffleData(data);
[trData,teData]=splitData(data);

m = size(teData,1); % size of the test data in one diamension
n = size(trData,1);
prediction=[];
for test = 1:m
    d=[];
    for train = 1:n
        d(train)=norm(teData(test,1:end-1)-trData(train,1:end-1)); 
    end
    [elt,index]=min(d); 
    predict(test) = trData(index,end);   % predict the label from trained one
end

actual = teData(:,end); % actual label of the data 

rate =100* sum(predict==actual')/m;

disp(rate);




    
    