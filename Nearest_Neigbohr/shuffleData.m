function data = shuffleData(data)
    [m,n] = size(data);             % size of the data row,col
    indices = randperm(m);          % random permutation of suffeled index
    data = data(indices,:);         % assing the data to shuffled index
    