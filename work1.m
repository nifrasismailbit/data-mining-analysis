data=load('./data/iris.txt');              % Loading the Data
data = shuffleData(data);           % Shuffling the Data
[trData, teData] = splitData(data); % Splitting the Data into 70:30

% Number of classes in the dataset
N = 3;

%Generate Class Numbers
A = 1:N;

%Paremeter of the linear kernal
C = 1;

%Linear Kernal
options = svmlopt('c', C);

%Learning
trX = invertData(data);

%trY is the class label
trY = trX(:,end);

trX(:,end) = [];

%Create the model
%Create the label to model
model = 'model2vsAll';

%write the data to svml understandable format
svmlwrite('svmlTrain',trX,trY);

%Train the system
svm_learn(options,'svmlTrain',model);

%##########################
%Testing
teX = invertData(data);

%teY is the class label
teY = teX(:,end);

teX(:,end) = [];

%write the data to svml understandable format
svmlwrite('svmlTest',teX,teY);

%model out
modelOut = 'modelOut2vsAll';

%classify the data
svm_classify(options,'svmlTest',model,modelOut);

%Read the prediction in our understandable format
prediction = svmlread(modelOut);

