function data = scaleData(data,lowlim,upplim)
    [r,c] = size(data);
    for j=1:c-1                                             %run from 1: c-1 exclude class label
        maxD = max(data(:,j));                              %Maximum colum of datas to be normalized
        minD = min(data(:,j));                              
        scale = (upplim-lowlim)/(maxD-minD);
        data(:,j) = scale*(data(:,j) - minD) + lowlim;
    end