function[trData,teData] = splitData(data)
    r = size(data,1);           % # of data
    m = round(r*0.7);           % # 70 percent of the data
    trData = data(1:m,:);       % # getting the trainning data
    data(1:m,:) = [];           % delete the getting 70 percent data
    teData = data;              % copy to teData
    